﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpoDarelBlondet
{
    public class Auto : Prototipo
    {
        private string modelo;
        private int costo;

        public string Modelo { get => modelo; set => modelo = value; }
        

        //constructor recibimos como parametros modelo y costo
        public Auto(string pModelo, int pCosto)
        {
            //asignamos a las variables internas
            modelo = pModelo;
            costo = pCosto;
        }

        public override string ToString()
        {
            return string.Format("Auto: {0}, {1}", modelo,costo);
        }

        //aprovechamos el constructor para llevar a cabo la copia del estado del objeto
        public object Clonar()
        {
            Auto clon = new Auto(modelo, costo);

            return clon;
        }

    }
}
