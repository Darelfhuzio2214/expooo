﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpoDarelBlondet
{
    //Simula que esta es una clase que tiene un constructor costoso
    class Valores:Prototipo
    {
        //declaramos dos variables que en este caso son privadas
        //sumatoria de tipo doble
        private double sumatoria;
        //m que es de tipo entero y que en este caso es 1
        private int m = 1;

        //Creamos 2 propiedades una para M y otra para sumatoria
        public int M { get => m; set => m = value; }
        public double Sumatoria { get => sumatoria; set => sumatoria = value; }

        internal Prototipo Prototipo
        {
            get => default;
            set
            {
            }
        }

        public Valores()
        {

        }

        //Este es el constructor costoso
        public Valores (int pM)
        {
            m = pM;
            int n = 0;
            for (n = 0; n < 90; n++)
                sumatoria += Math.Sin(n * 0.0175);
        }

        public override string ToString()
        {
            return string.Format(" {0} ", sumatoria * m);            
        }
        
        public object Clonar()
        {
            //para poder clonar creamos una instancia
            Valores clon = new Valores();
            clon.M = m;
            clon.Sumatoria = sumatoria;

            return clon;
        }

    }
}
