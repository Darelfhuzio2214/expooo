﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ExpoDarelBlondet
{
    public class Personitas : Prototipo
    {

        private string nombre;
        private int edad;

        public string Nombre { get => nombre; set => nombre = value; }


        //en el constructor recibiremos 2 parametros que son nombre y edad
        public Personitas (string pNombre, int pEdad)
        {
            //asignamos a las variables internas
            nombre = pNombre;
            edad = pEdad;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1} ", nombre, edad);
        }

        //aprovechamos el constructor para llevar a cabo la copia del estado del objeto
        public object Clonar()
        {
            Personitas clon = new Personitas(nombre, edad);

            return clon;
        }
    }
}
