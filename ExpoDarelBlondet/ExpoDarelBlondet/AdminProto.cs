﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpoDarelBlondet
{
    class AdminProto
    {
        private Dictionary<string, Prototipo> prototipo = new Dictionary<string, Prototipo>();

        public AdminProto()
        {

            //Adicionamos los objetos prototipo con los vaslores iniciales que nos interesen

            Personitas persona = new Personitas("Ciudadano", 18);
            prototipo.Add("Persona", persona);

            Valores valores = new Valores(1);
            prototipo.Add("Valores", valores);
        }

        public void AdicionaPrototipo(string pLLave, Prototipo pPrototipo)
        {
            prototipo.Add(pLLave, pPrototipo);
        }

        public object ObtenerPrototipo(string pLLave)
        {
            return prototipo[pLLave].Clonar();

        }
    }
}
