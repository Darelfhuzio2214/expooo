﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpoDarelBlondet
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creamos el administrador
            AdminProto admin = new AdminProto();

            //Obtenemos dos instancias 
            Personitas uno = (Personitas)admin.ObtenerPrototipo("Persona");
            Personitas dos = (Personitas)admin.ObtenerPrototipo("Persona");

            //Verificamos que tengan los valores del prototipo original
            Console.WriteLine(uno);
            Console.WriteLine(dos);
            Console.WriteLine("------------------------");

            //Modificamos el estado
            uno.Nombre = "José";
            dos.Nombre = "David";

            //Verificamos que cada quien tenga su estado
            Console.WriteLine(uno);
            Console.WriteLine(dos);
            Console.WriteLine("------------------------");

            //Creamos una instancia
            Auto auto = new Auto("Nissan", 250000);

            //Lo colocamos como propietario
            admin.AdicionaPrototipo("Auto", auto);

            //Obtenemos un objeto de ese prototipo
            Auto auto2 = (Auto)admin.ObtenerPrototipo("Auto");

            //Cambio el estado
            auto2.Modelo = "Honda";

            //Verificamos que cada uno tenga su estado
            Console.WriteLine(auto);
            Console.WriteLine(auto2);
            Console.WriteLine("------------------------");

            //Obtenemos una instancia del costoso
            Valores val = (Valores)admin.ObtenerPrototipo("Valores");

            Console.WriteLine(val);

        }
    }
}
